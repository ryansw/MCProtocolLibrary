package com.ryansw.mcprotocollib;

/**
 * The VarInt class is a wrapper for conversion between standard
 * integers and 'varint'(s) used for many network protocols.
 * 
 * @author Ryan S. White <ryanswhite98@gmail.com>
 *
 */
public class VarInt {

	/**
	 * The byte-encoded varint data
	 */
	public byte[] data;

	/**
	 * Construct a new VarInt using byte-encoded varint data.
	 * This function does *NOT* check that the data coming in
	 * follows the proper varint format. It does *NOT* look at the
	 * 0x80 bit and assumes that the correct amount of bytes has
	 * been pre-determined.
	 * 
	 * @param data The byte-encoded varint data to construct a new VarInt
	 */
	public VarInt(byte[] data) {
		this.data = data;
	}

	/**
	 * Construct a new VarInt using an integer.
	 * 
	 * @param number The integer to format as a VarInt
	 */
	public VarInt(int number) {
		int use = (int) (Math.log(number) / Math.log(128)) + 1;
		data = new byte[use];
		for (int a = 0; a < use; a++) {
			data[a] = (byte) (number & 0x7F);
			number >>= 7;
		}
		for (short a = 0; a < use - 1; a++)
			data[a] += 128;
	}

	/**
	 * Gives the integer value of a VarInt
	 * 
	 * @return Integer value of VarInt
	 */
	public int getInt() {
		int ret = 0;
		for (int a = data.length - 1; a >= 0; a--) {
			ret <<= 7;
			ret += data[a] & 0x7F;
		}
		return ret;
	}

	/**
	 * Gives the raw byte-encoded VarInt data
	 * 
	 * @return byte-encoded varint
	 */
	public byte[] getBytes() {
		return data;
	}
}
